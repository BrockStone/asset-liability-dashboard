import React, { useContext } from "react";
import { store } from "../Store.js";
import NavigationBar from "../components/NavigationBar.js";

export default function AssetLiabilityTable() {
  const { setModalVisibility, assetsAndLiabilites } = useContext(store);
  return (
    <NavigationBar
      setModalVisibility={setModalVisibility}
      assetsAndLiabilites={assetsAndLiabilites}
    />
  );
}
