# Asset & Liability Manager

![Application screenshot](./src/assets/images/screenshot.png)

This project was created to keep track of a users Assets/Liabilities and provide valuable insights based on entries (asset total, liability total and net worth)

## Available Scripts

In the project directory, you can run:

### `start-dev`

Simultaneously starts the SQLite server and runs the app in the development mode<br  />

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br  />

You will also see any lint errors in the console.

### `start-server`

Starts the SQLite server.

### `start-front`

Starts the app in the development mode.

### Deployment to Heroku

If cloning project - Use Git to clone asset-liability-manager's source code to your local machine.

```javascript

$  heroku  git:clone  -a  asset-liability-manager

$  cd  asset-liability-manager

```

Make some changes to the code you just cloned and deploy them to Heroku using Git.

```javascript

$  git  push  heroku  master

```

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).
