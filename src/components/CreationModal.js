import React, { useState } from "react";
import Modal from "./Modal";
import {
  faCircle,
  faDotCircle,
  faExclamationCircle
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const itemTypes = ["asset", "liability"];

export default function CreationModal({
  setModalVisibility,
  modalVisible,
  handleAssetLiabilityCreation
}) {
  const initialState = {
    type: "asset",
    name: "",
    balance: ""
  };

  // Sets item form data to state
  const [itemData, setItemData] = useState(initialState);
  const [hasError, setError] = useState(false);

  const resetForm = () => {
    setItemData(initialState);
    setError(false);
  };

  // Validate form entries
  const validate = async () => {
    if (!itemData.name || !itemData.balance) {
      setError(true);
    } else {
      await handleAssetLiabilityCreation(itemData);
      resetForm();
    }
  };

  // Handle Enter form submit
  const keyPressed = event => {
    if (event.key === "Enter") {
      validate();
    }
  };

  return (
    <Modal
      visible={modalVisible}
      handleClose={() => {
        setModalVisibility(false);
        resetForm();
      }}
      title="+ Create new"
    >
      <div className="modal-content">
        {/* Choose new item type (asset or liability) */}
        <div className="form-item">
          <label>Choose Type</label>

          {itemTypes.map(type => (
            <div
              key={type}
              className={`item-type ${itemData.type === type && "active"}`}
              onClick={() => setItemData({ ...itemData, type: type })}
            >
              <FontAwesomeIcon
                icon={itemData.type === type ? faDotCircle : faCircle}
              />{" "}
              {type}
            </div>
          ))}
        </div>

        {/* Set items name */}
        <div className="form-item">
          <label>Name</label>
          <input
            type="text"
            placeholder={`Please specify a name for your ${itemData.type}`}
            name="name"
            value={itemData.name}
            onKeyPress={e => keyPressed(e)}
            onChange={e =>
              setItemData({
                ...itemData,
                [e.target.name]: e.target.value
              })
            }
          />
        </div>

        {/* Set item balance */}
        <div className="form-item">
          <label>Balance</label>
          <input
            type="number"
            min="1"
            placeholder="Current balance?"
            name="balance"
            value={itemData.balance}
            onKeyPress={e => keyPressed(e)}
            onChange={e =>
              setItemData({
                ...itemData,
                [e.target.name]: e.target.value
              })
            }
          />
        </div>
      </div>
      <div className="modal-footer">
        {hasError && (
          <span>
            <FontAwesomeIcon icon={faExclamationCircle} /> Please complete all
            fields to proceed.
          </span>
        )}
        <button className="button primary" onClick={() => validate()}>
          + Add {itemData.type}
        </button>
      </div>
    </Modal>
  );
}
