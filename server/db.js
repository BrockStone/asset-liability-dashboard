// Import path module
const path = require("path");

// Set path location - (database.sqlite)
const dbPath = path.resolve(__dirname, "db/database.sqlite");

// Connection to SQLite database
const knex = require("knex")({
  client: "sqlite3",
  connection: {
    filename: dbPath
  },
  useNullAsDefault: true // <---- Replaces undefined keys with NULL instead of DEFAULT
});

// Create the assetsLiabilities table
knex.schema
  // Prior to creation - make sure the "assetsLiabilities" table doesn't already exist
  .hasTable("assetsLiabilities")
  .then(exists => {
    // If no "assetsLiabilities" table exists create new table
    if (!exists) {
      // Columns:
      // ID - increments with every new record
      // Name - record descriptor
      // Type - asset or liability
      // Balance - current record balance
      return knex.schema
        .createTable("assetsLiabilities", t => {
          t.increments("id").primary();
          t.string("name");
          t.string("type");
          t.float("balance");
        })
        .then(() => {
          console.log("Successfully created the assets and liabilities table");
        })
        .catch(error => {
          console.error(`There was an error creating table: ${error}`);
        });
    }
  })
  .then(() => {
    console.log("Done. Database has been created.");
  })
  .catch(error => {
    console.error(`There was an error setting up the database: ${error}`);
  });

// Export DB
module.exports = knex;
