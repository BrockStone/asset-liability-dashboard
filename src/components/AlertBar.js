import React from "react";

export default function AlertBar({
  visible,
  type,
  message = "Hey, there!",
  setAlertState
}) {
  if (visible) {
    setTimeout(() => {
      setAlertState({ visible: false });
    }, 4000);
  }
  return (
    <div
      className={`alert-bar ${type || "default"} animate ${
        visible ? "fadeIn" : "display-none"
      }`}
    >
      {message}
    </div>
  );
}
