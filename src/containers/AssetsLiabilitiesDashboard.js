import React, { useContext } from "react";
import { store } from "../Store.js";
import CreationModal from "../components/CreationModal";
import Table from "../components/Table";
import Placeholder from "../components/Placeholder";
import Statistics from "../components/Statistics";
import Loader from "../components/Loader.js";
import AlertBar from "../components/AlertBar.js";

export default function AssetsLiabilitiesDashboard() {
  const {
    setModalVisibility,
    modalVisible,
    assetsAndLiabilites,
    handleAssetLiabilityCreation,
    handleAssetLiabilityRemoval,
    stats,
    loader,
    alert,
    setAlertState
  } = useContext(store);

  return (
    <div className="animate fadeIn">
      <AlertBar
        visible={alert.isVisible}
        message={alert.message}
        type={alert.type}
        setAlertState={setAlertState}
      />
      <Loader visible={loader.isLoading} message={loader.message} />
      <CreationModal
        setModalVisibility={setModalVisibility}
        modalVisible={modalVisible}
        handleAssetLiabilityCreation={handleAssetLiabilityCreation}
      />
      {assetsAndLiabilites.length && !loader.isLoading ? (
        <>
          <Statistics stats={stats} />
          <Table
            data={assetsAndLiabilites}
            handleAssetLiabilityRemoval={handleAssetLiabilityRemoval}
          />
        </>
      ) : (
        !loader.isLoading && (
          <Placeholder
            title="You have not added any assets or liabilities."
            message="Once you do, they will appear here."
            action={{
              label: "+Add New",
              method: () => setModalVisibility(true)
            }}
          />
        )
      )}
    </div>
  );
}
