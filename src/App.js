import React from "react";
import "./App.css";

// Import Application Components
import NavigationBar from "./containers/NavigationBar";
import AssetsLiabilitiesDashboard from "./containers/AssetsLiabilitiesDashboard";

// Import Store
import { AssetLiabilityProvider } from "./Store.js";

function App() {
  return (
    <AssetLiabilityProvider>
      <div className="App">
        <NavigationBar />
        <AssetsLiabilitiesDashboard />
      </div>
    </AssetLiabilityProvider>
  );
}

export default App;
