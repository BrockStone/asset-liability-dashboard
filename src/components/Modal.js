import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const Modal = ({
  visible,
  handleClose,
  size = "default",
  title = "modal",
  children,
  type = "default",
  icon = null
}) => {
  const showHideClassName = visible
    ? "modal display-block"
    : "modal display-none";

  return (
    <div className={showHideClassName}>
      <div className={`modal-container ${size}`}>
        <div className={`modal-header ${type === "error" && "error"}`}>
          <div className="title">
            {icon && <FontAwesomeIcon icon={icon} />} {title}
          </div>
          <div className="close" onClick={() => handleClose()}>
            <FontAwesomeIcon icon="times" />
          </div>
        </div>
        {children}
      </div>
    </div>
  );
};

export default Modal;
