const express = require("express");
const cors = require("cors");
const helmet = require("helmet");
const bodyParser = require("body-parser");
const compression = require("compression");
const path = require("path");

// Import routes
const assetsLiabilitiesRouter = require("./routes/assets-liabilities-route");

// Sets default port for express app
const PORT = process.env.PORT || 5000;

// Create express app
const app = express();

// Serve the static files from the React app
app.use(express.static(path.join(__dirname, "../build")));

// Applys middleware
app.use(cors());
app.use(helmet());
app.use(compression());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// Implement assets and liabilities route
app.use("/api/assetsLiabilities", assetsLiabilitiesRouter);

// 404 route
app.use((req, res, next) => {
  res.status(404).send("Sorry, not found.");
});

// 500 route
app.use(function(err, req, res, next) {
  console.error(err.stack);
  res.status(500).send("Oops..There was an issue.");
});

// Start express app
app.listen(PORT, () => console.log(`Server is running on PORT: ${PORT}`));
