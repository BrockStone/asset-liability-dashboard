import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export default function Loader({ visible, message = "Loading" }) {
  return (
    <div id="loader" className={visible ? "display-block" : "display-none"}>
      <FontAwesomeIcon icon="spinner" size="lg" pulse />
      <h3>{message}</h3>
    </div>
  );
}
