// Import SQLite Db instance
const knex = require("../db");

// Retrieve all assets and liabilities
exports.assetsLiabilitiesAll = async (req, res) => {
  knex
    .select("*")
    .from("assetsLiabilities")
    .then(data => {
      res.json(data); // <-- Sends ALL assets and liabilities from database in response
    })
    // Catch error
    .catch(err => {
      res.json({
        message: `There was an error retrieving your assets and liabilities: ${err}`
      });
    });
};

// Create new asset or liability
exports.assetsLiabilitiesCreate = async (req, res) => {
  knex("assetsLiabilities")
    .insert({
      type: req.body.type,
      name: req.body.name,
      balance: req.body.balance
    })
    .then(() => {
      res.json({
        message: `"${req.body.name}" ${req.body.type} created.`
      });
    })
    // Catch error
    .catch(err => {
      res.json({
        message: `There was an error creating the "${req.body.name}" ${req.body.type}: ${err}`
      });
    });
};

// Remove asset or liability - (by ID)
exports.assetsLiabilitiesDelete = async (req, res) => {
  knex("assetsLiabilities")
    .where("id", req.body.id)
    .del()
    .then(() => {
      res.json({ message: `"${req.body.name}" ${req.body.type} removed.` });
    })
    // Catch error
    .catch(err => {
      res.json({
        message: `There was an error removing the "${req.body.name}" ${req.body.type}: ${err}`
      });
    });
};

// Retrieve Stats - (Net Worth, Asset total, Liability total)
exports.assetsLiabilitiesStats = async (req, res) => {
  knex("assetsLiabilities")
    .where({
      type: "asset"
    })
    .sum({ asset_total: "balance" })
    .then(async assets => {
      knex("assetsLiabilities")
        .where({
          type: "liability"
        })
        .sum({ liability_total: "balance" })
        .then(async liabilities => {
          res.json({
            ...assets[0],
            ...liabilities[0],
            net_worth: assets[0].asset_total - liabilities[0].liability_total
          });
        });
    })

    // Catch error
    .catch(err => {
      res.json({
        message: `There was an error retrieving your Stats: ${err}`
      });
    });
};
