import React, { createContext, useState, useEffect } from "react";
import axios from "./AxiosConfig";

const initialState = {
  assetsAndLiabilites: [],
  modalVisible: false,
  loader: {
    isloading: false,
    message: "Loading"
  },
  alert: {
    isVisible: false,
    type: "success",
    message: "Alert!"
  },
  stats: {}
};

const store = createContext(initialState);
const { Provider } = store;

const AssetLiabilityProvider = ({ children }) => {
  const [assetsAndLiabilites, setAssetsAndLiabilites] = useState(
    initialState.assetsAndLiabilites
  );
  const [stats, setStats] = useState(initialState.stats);
  const [modalVisible, setModalVisibility] = useState(
    initialState.modalVisible
  );
  const [loader, setLoadingState] = useState(initialState.loader);
  const [alert, setAlertState] = useState(initialState.alert);

  // Handle Errors
  const handleError = errorMsg => {
    setLoadingState({ isLoading: false });
    setAlertState({
      isVisible: true,
      type: "error",
      message: errorMsg
    });
    console.error(errorMsg);
  };

  // Fetch Assets + liabilities list
  const fetchAssetsLiabilities = async () => {
    setLoadingState({ isLoading: true, message: "Fetching your dashboard" });
    try {
      const al = await axios.get("/assetsLiabilities/all");
      setAssetsAndLiabilites(al.data);
      setLoadingState({ isLoading: false });
    } catch (error) {
      setLoadingState({ isLoading: false });
      handleError(`Error fetching your assets and liabilities: ${error}`);
    }
  };

  // Fetch Stats
  const fetchStats = async () => {
    setLoadingState({ isLoading: true, message: "Fetching your Stats" });
    try {
      const stats = await axios.get("/assetsLiabilities/stats");
      setStats(stats.data);
      setLoadingState({ isLoading: false });
    } catch (error) {
      setLoadingState({ isLoading: false });
      handleError(`There was an error fetching stats: ${error}`);
    }
  };

  // Create new asset or liability
  const handleAssetLiabilityCreation = ({ name, type, balance }) => {
    setLoadingState({ isLoading: true, message: `Saving "${name}" ${type}` });
    setModalVisibility(false);
    let absBalance = Math.abs(balance); // <--- converts any (-) negative balance (+) positive
    axios
      .post("/assetsLiabilities/create", {
        name: name,
        type: type,
        balance: absBalance
      })
      .then(async () => {
        setAlertState({
          isVisible: true,
          type: "success",
          message: `Successfully created the "${name}" ${type}.`
        });
        await fetchAssetsLiabilities(); // <--- refetch assets and liabilities
        await fetchStats(); // <--- refetch stats
        setLoadingState({ isLoading: false });
      })
      .catch(error =>
        handleError(
          `There was an error creating the "${name}" ${type}: ${error}`
        )
      );
  };

  // Remove asset or liability
  const handleAssetLiabilityRemoval = ({ id, name, type }) => {
    setLoadingState({ isLoading: true, message: `Removing "${name}" ${type}` });
    axios
      .put("/assetsLiabilities/delete", {
        id: id,
        name: name,
        type: type
      })
      .then(async () => {
        setAlertState({
          isVisible: true,
          type: "success",
          message: `Successfully removed the "${name}" ${type}.`
        });
        await fetchAssetsLiabilities(); // <--- refetch assets and liabilities
        await fetchStats();
        setLoadingState({ isLoading: false });
      })
      .catch(error =>
        handleError(
          `There was an error creating the "${name}" ${type}: ${error}`
        )
      );
  };

  // Fetch Assets + Liabilities and Stats
  useEffect(() => {
    fetchAssetsLiabilities();
    fetchStats();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <Provider
      value={{
        assetsAndLiabilites,
        setAssetsAndLiabilites,
        modalVisible,
        setModalVisibility,
        loader,
        alert,
        setAlertState,
        handleAssetLiabilityCreation,
        handleAssetLiabilityRemoval,
        stats
      }}
    >
      {children}
    </Provider>
  );
};

export { store, AssetLiabilityProvider };
