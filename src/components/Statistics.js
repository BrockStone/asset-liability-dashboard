import React from "react";
import StatCard from "./StatCard";

export default function Statistics({ stats }) {
  return (
    <div id="stats-container">
      {Object.keys(stats).map(stat => (
        <StatCard
          key={`${stat}-${stats[stat]}`}
          type={stat}
          value={stats[stat]}
        />
      ))}
    </div>
  );
}
