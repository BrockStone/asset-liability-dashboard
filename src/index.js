import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import * as serviceWorker from "./serviceWorker";

// font awesome imports
import { library } from "@fortawesome/fontawesome-svg-core";

import { faTimes, faSpinner } from "@fortawesome/free-solid-svg-icons";

// Library to Reference Icons Throughout App
library.add(faTimes, faSpinner);

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
