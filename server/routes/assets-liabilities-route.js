const express = require("express");

// Import assets-liabilities-controller
const alc = require("../controllers/assets-liabilities-controller.js");

// Create router
const router = express.Router();

// Route for GET request to retrieve all assets and liabilities
router.get("/all", alc.assetsLiabilitiesAll);

// Route for POST request to create new asset or liability
router.post("/create", alc.assetsLiabilitiesCreate);

// Route for PUT request to delete specific asset or liability by ID
router.put("/delete", alc.assetsLiabilitiesDelete);

// Routes for GET request to retrieve stats (Net Worth, total assets and total liabilities)
router.get("/stats", alc.assetsLiabilitiesStats);

// Export router
module.exports = router;
