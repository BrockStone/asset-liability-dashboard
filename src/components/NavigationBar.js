import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSearchDollar } from "@fortawesome/free-solid-svg-icons";

export default function NavigationBar({
  setModalVisibility,
  assetsAndLiabilites
}) {
  return (
    <div id="main-navigation-bar">
      <h4>
        <FontAwesomeIcon icon={faSearchDollar} /> Asset &amp; Liability Manager
      </h4>
      {assetsAndLiabilites.length >= 1 && (
        <button
          className="button primary"
          onClick={() => setModalVisibility(true)}
        >
          + Add new
        </button>
      )}
    </div>
  );
}
