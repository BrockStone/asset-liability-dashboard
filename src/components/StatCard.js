import React from "react";
import { startCase } from "lodash";
import CountUp from "react-countup";
import { formatNumber } from "../utilities";

export default function StatCard({ type, value }) {
  return (
    <div className="stat-card">
      <div className="title">{startCase(type)}</div>
      <div
        className={`value ${type === "net_worth" && "net"} ${value < 0 &&
          "negative"}`}
      >
        <CountUp
          formattingFn={formatNumber}
          end={value || 0}
          decimals={2}
          duration={0.4}
        />
      </div>
    </div>
  );
}
