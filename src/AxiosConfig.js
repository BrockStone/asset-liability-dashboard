import axios from "axios";

// Create Axios Instance
const instance = axios.create({
  baseURL: "/api",
  headers: { Pragma: "no-cache" } // <-- Avoids caching in Internet Explorer
});

export default instance;
