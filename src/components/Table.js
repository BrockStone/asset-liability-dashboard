import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTrash, faCircle } from "@fortawesome/free-solid-svg-icons";
import { formatNumber } from "../utilities";
import { startCase } from "lodash";

export default function Table({ data, handleAssetLiabilityRemoval }) {
  // Remove table item (with confirmation)
  const removeItem = item => {
    if (
      window.confirm(
        `Are you sure you'd like to remove the "${item.name}" ${item.type}?`
      )
    ) {
      handleAssetLiabilityRemoval(item);
    }
  };

  return (
    <div id="assets-liabilities-container">
      <h3>Your Assets &amp; Liabilities</h3>
      <table id="assets-liabilities-table">
        <tbody>
          <tr>
            <th>Name</th>
            <th>Type</th>
            <th>Balance</th>
          </tr>
          {data.map((item, index) => (
            <tr key={item.name + index}>
              <td>{item.name}</td>
              <td>
                <FontAwesomeIcon
                  icon={faCircle}
                  size="xs"
                  style={{
                    color: item.type === "asset" ? "#00ffbc" : "#f36b6b"
                  }}
                />{" "}
                {startCase(item.type)}
              </td>
              {item.type === "asset" && <td>{formatNumber(item.balance)}</td>}
              {item.type !== "asset" && <td>({formatNumber(item.balance)})</td>}
              <td className="remove-col" onClick={() => removeItem(item)}>
                <FontAwesomeIcon icon={faTrash} /> Remove
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}
