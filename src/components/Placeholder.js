import React from "react";

export default function Placeholder({ title, message, action }) {
  return (
    <div className="placeholder">
      <h3>{title}</h3>
      <p>{message}</p>
      <button className="button primary" onClick={() => action.method()}>
        {action.label}
      </button>
    </div>
  );
}
